<?php

namespace Waldhacker\Oauth2Client\Exception;

use Exception;

class MissingConfigurationException extends Exception
{
}
